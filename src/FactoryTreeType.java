import java.util.HashMap;

public class FactoryTreeType {

    private HashMap<Integer, TreeType> treeTypeHashMap = new HashMap<>();

    public TreeType getTreeType(int numberType){
        TreeType treeType = treeTypeHashMap.get(numberType);

        if (treeType == null){
            switch (numberType) {
                case 1 : {
                    treeType = new TreeType("Oak", "Brown");
                    break;
                }
                case 2 : {
                    treeType = new TreeType("Aspen", "Green");
                    break;
                }
                case 3 : {
                    treeType = new TreeType("BirchTree", "DarkGreen");
                    break;
                }
                case 4 : {
                    treeType = new TreeType("Poplar", "LightGreen");
                    break;
                }
                case 5 : {
                    treeType = new TreeType("Linden", "Yellow");
                    break;
                }
            }
            treeTypeHashMap.put(numberType, treeType);
        }
        return treeType;
    }
}
