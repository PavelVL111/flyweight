import java.util.ArrayList;
import java.util.List;

public class Demo {
    public static void main(String[] args) {

        printForest(createForest());

    }

    private static List<Tree> createForest(){
        List<Tree> trees = new ArrayList<>();
        FactoryTreeType factoryTreeType = new FactoryTreeType();
        for (int i = 0; i < 100; i++) {
            trees.add(new Tree(getRandomX(), getRandomY(), factoryTreeType.getTreeType(getRandomNumberType())));
        }
        return trees;
    }

    private static int getRandomNumberType() {
        return (int) Math.round(Math.random()*4 + 1);
    }

    private static int getRandomX() {
        return (int) Math.round(Math.random()*100);
    }

    private static int getRandomY() {
        return (int) Math.round(Math.random()*100);
    }

    private static void printForest(List<Tree> trees){
        trees.stream().forEach(tree -> tree.print());
    }

}
