
public class TreeType {

    private String typeName;

    private String color;

    public TreeType(String typeName, String color) {
        this.typeName = typeName;
        this.color = color;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

}
